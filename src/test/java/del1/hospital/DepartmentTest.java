package del1.hospital;

import del1.hospital.exception.RemoveException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Testing class for department.remove()
 * @author Eivind Strand Harboe
 */

class DepartmentTest {

    /**
     * Method for checking if remove() throws RemoveException when there is no Person object with coresponding id
     */

    @Test
    void removeNonExistingPeron() {
        Department dep = new Department("Department");
        Person person = new Patient("Eivind", "Strand", "040300");
        assertThrows(RemoveException.class, () -> { dep.remove(person); });
    }

    /**
     * Test method for checking if the removal of a person from a register works
     */

    @Test
    void removePatient(){
        Department dep = new Department("Department");
        Patient person =new Patient("Eivind","Strand","040300");
        dep.addPatients(person);
        try{
            dep.remove(person);
        }catch (RemoveException e){
            System.out.println(e.getMessage());
        }
        assertFalse(dep.getPatients().containsKey(Integer.parseInt(person.getSocialSecurityNumber())));
    }

    /**
     * Test method for checking the removal of an employee
     */

    @Test
    void removeEmployee(){
        Department dep = new Department("Department");
        Employee employee = new Employee("Adrian", "Wist Hakvåg","050200");
        dep.addEmployee(employee);
        try {
            dep.remove(employee);
        }catch (RemoveException e){
            System.out.println(e.getMessage());
        }
        assertFalse(dep.getEmployees().containsKey(Integer.parseInt(employee.getSocialSecurityNumber())));
    }

    /**
     * Method for checking if removing null returns NullPointerException
     */

    @Test
    void removeNull(){
        Department dep = new Department("Department");
        Employee employee = null;
        String s = null;
        assertThrows(NullPointerException.class, () ->{ dep.remove(employee); } );
    }
}