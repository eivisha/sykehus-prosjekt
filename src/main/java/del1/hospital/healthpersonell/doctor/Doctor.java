package del1.hospital.healthpersonell.doctor;

import del1.hospital.Employee;
import del1.hospital.Patient;

public abstract class Doctor extends Employee {
    public Doctor(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }
    public abstract void setDiagnosis(Patient patient, String diagnosis);

}
