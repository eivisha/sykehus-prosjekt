package del1.hospital;

import del1.hospital.exception.RemoveException;

import java.util.HashMap;
import java.util.Objects;

/**
 * This class represents a department
 * @author Eivind Strand Harboe
 * @version 1.0
 */

public class Department {
    private String departmentName;
    private HashMap<String,Employee> employees;
    private HashMap<String,Patient> patients;

    /**
     * Constructor
     * @param departmentName
     */

    public Department(String departmentName){
        this.departmentName=departmentName;
        employees=new HashMap<>();
        patients=new HashMap<>();
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    /**
     * Returns all employees
     * @return
     */
    public HashMap<String,Employee> getEmployees(){
        HashMap<String, Employee> allEmployees= new HashMap<>();
        for (Employee e:employees.values()){
            allEmployees.put(e.getSocialSecurityNumber(), e);
        }
        return allEmployees;
    }

    /**
     * Adds a new employee
     * @param employee
     */
    public void addEmployee(Employee employee){
        if (employee!=null){
            employees.put(employee.getSocialSecurityNumber(),employee);
        }
    }

    /**
     * Returns patients
     * @return
     */

    public HashMap<String, Patient> getPatients() {
        HashMap<String, Patient> allPatients= new HashMap<>();
        for (Patient p:patients.values()){
            allPatients.put(p.getSocialSecurityNumber(),p);
        }
        return allPatients;
    }

    /**
     * Adds a new patient
     * @param patient
     */

    public void addPatients(Patient patient){
        if (patient!=null){
            patients.put(patient.getSocialSecurityNumber(),patient);
        }
    }

    /**
     * Equals method
     * @param o
     * @return
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return Objects.equals(departmentName, that.departmentName);
    }

    /**
     * Method for removing a person. Checks for which type of person and removes the person from the according list
     * If no person is found or the person is not a instance of employee or patient a remove exception gets thrown
     * @param person
     * @throws RemoveException
     */

    public void remove(Person person) throws RemoveException {
        if (person instanceof Employee){
            if (employees.containsKey(person.getSocialSecurityNumber())){
                employees.remove(person.getSocialSecurityNumber());
            }else{throw new RemoveException("Person did not exist");}
        }
        else if (person instanceof Patient){
            if (patients.containsKey(person.getSocialSecurityNumber())){
                patients.remove(person.getSocialSecurityNumber());
            }else{throw new RemoveException("Person did not exist");}
        }
        else{
            throw new RemoveException("Could not remove " + person.getFirstName());
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(departmentName, employees, patients);
    }
}
