package del1.hospital;

import del1.hospital.exception.RemoveException;

import java.util.HashMap;

public class HospitalClient {
    public static void main(String[] args) throws RemoveException {

        /**
         * Creates a hospital, fills it with data. Copies employees and removes on of the employees.
         * Updates the list and shows it to users
         */

        Hospital hospital=new Hospital("Bærum Sykehus");
        HospitalTestData.fillRegisterWithTestData(hospital);
        HashMap<String, Employee> employees = hospital.getDepartments().get(0).getEmployees();
        System.out.println("Employees in "+hospital.getDepartments().get(0).getDepartmentName()+":");
        for(Employee e: employees.values()){
            System.out.println(e.toString());
        }
        hospital.getDepartments().toString();
        Employee employee1=hospital.getDepartments().get(0).getEmployees().get("2");
        System.out.println("\nRemoving employee number 2\n");
        try{
            hospital.getDepartments().get(0).remove(employee1);
        }catch (RemoveException e){
            System.out.println(e.getMessage());
        }

        employees = hospital.getDepartments().get(0).getEmployees();
        System.out.println("Employees in "+hospital.getDepartments().get(0).getDepartmentName()+":");
        for(Employee e: employees.values()){
            System.out.println(e.toString());
        }

        /**
         * Tries to remove an element that doesnt exist
         */


        try{
            System.out.println("\nRemoving person that doesnt exist\n");
            hospital.getDepartments().get(0).remove(new Employee("Rigmor", "Mortis", "118"));
        }catch (RemoveException e){
            System.out.println(e.getMessage());
        }

        /**
         * Copies patients from deparments and removes one of the patients
         * Updates the list and prints it to the user
         */


        HashMap<String, Patient> patients=hospital.getDepartments().get(1).getPatients();
        System.out.println("\nPatients in "+hospital.getDepartments().get(1).getDepartmentName()+":");
        for(Patient p: patients.values()){
            System.out.println(p.toString());
        }
        System.out.println("\nRemoving patient 17 from children policlinic\n");
        Patient patient=hospital.getDepartments().get(1).getPatients().get("17");

        try{
            hospital.getDepartments().get(1).remove(patient);
        }catch (RemoveException e){
            System.out.println(e.getMessage());
        }
        patients=hospital.getDepartments().get(1).getPatients();
        System.out.println("\nPatients in "+hospital.getDepartments().get(1).getDepartmentName()+":");
        for(Patient p: patients.values()){
            System.out.println(p.toString());
        }
    }
}
