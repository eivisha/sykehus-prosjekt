package del1.hospital;


import java.util.ArrayList;

/**
 * Class that represents a hospital, keeps track of all departments
 */

public class Hospital {
    private String hospitalName;
    private ArrayList<Department>departments;

    /**
     * Sets hospital name
     * @param hospitalName
     */


    public Hospital(String hospitalName){
        this.hospitalName=hospitalName;
        departments=new ArrayList<>();
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public ArrayList<Department> getDepartments() {
        return departments;
    }

    public void addDepartment(Department department){
        departments.add(department);
    }

    @Override
    public String toString() {
        return
                "Hospital Name =" + hospitalName;
    }
}
