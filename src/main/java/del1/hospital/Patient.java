package del1.hospital;

public class Patient extends Person implements Diagnosable{
    String diagnosis="";

    /**
     * Constructor
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */

    public Patient(String firstName, String lastName, String socialSecurityNumber){
        super(firstName, lastName, socialSecurityNumber);
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    @Override
    public String toString() {
        return super.toString();
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }
}