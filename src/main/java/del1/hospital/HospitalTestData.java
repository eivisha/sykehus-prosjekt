package del1.hospital;

import del1.hospital.healthpersonell.Nurse;
import del1.hospital.healthpersonell.doctor.GeneralPractitioner;
import del1.hospital.healthpersonell.doctor.Surgeon;

public final class HospitalTestData {
    private HospitalTestData() {
        // not called
    }
    /**
     * @param hospital
     */
    public static void fillRegisterWithTestData(final Hospital hospital) {
        // Add some departments
        Department emergency = new Department("Akutten");
        emergency.addEmployee(new Employee("Odd Even", "Primtallet", "1"));
        emergency.addEmployee(new Employee("Huppasahn", "DelFinito", "2"));
        emergency.addEmployee(new Employee("Rigmor", "Mortis", "3"));
        emergency.addEmployee(new GeneralPractitioner("Inco", "Gnito", "4"));
        emergency.addEmployee(new Surgeon("Inco", "Gnito", "5"));
        emergency.addEmployee(new Nurse("Nina", "Teknologi", "6"));
        emergency.addEmployee(new Nurse("Ove", "Ralt", "7"));
        emergency.addPatients(new Patient("Inga", "Lykke", "8"));
        emergency.addPatients(new Patient("Ulrik", "Smål", "9"));
        hospital.addDepartment(emergency);
        Department childrenPolyclinic = new Department("Barn poliklinikk");
        childrenPolyclinic.addEmployee(new Employee("Salti", "Kaffen", "10"));
        childrenPolyclinic.addEmployee(new Employee("Nidel V.", "Elvefølger", "11"));
        childrenPolyclinic.addEmployee(new Employee("Anton", "Nym", "12"));
        childrenPolyclinic.addEmployee(new GeneralPractitioner("Gene", "Sis", "13"));
        childrenPolyclinic.addEmployee(new Surgeon("Nanna", "Na", "14"));
        childrenPolyclinic.addEmployee(new Nurse("Nora", "Toriet", "15"));
        childrenPolyclinic.addPatients(new Patient("Hans", "Omvar", "16"));
        childrenPolyclinic.addPatients(new Patient("Laila", "La", "17"));
        childrenPolyclinic.addPatients(new Patient("Jøran", "Drebli", "18"));
        hospital.addDepartment(childrenPolyclinic);
    }
}