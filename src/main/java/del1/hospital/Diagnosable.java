package del1.hospital;

/**
 * Indicates that an object can be diagnosedæ
 */

public interface Diagnosable {
    public void setDiagnosis(String diagnosis);
}
