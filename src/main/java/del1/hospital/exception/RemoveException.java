package del1.hospital.exception;

/**
 * Remove exception, extends Throwable
 */

public class RemoveException extends Throwable {
    private long serialVersionUID = 1L;

    public RemoveException(String message) {
        super(message);
    }
}
